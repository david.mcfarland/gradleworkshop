/**
 * Copyright 2014 Instil.
 */
package co.instil.bloom;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Test Hasher - very slow integration tests....
 */
public class HasherTest {

    @Test
    public void shouldGenerateCorrectTest() {
        assertThat(new Hasher().murmur3Hash("Blue").toString(), is(equalTo("incorrectHash")));
    }
}
