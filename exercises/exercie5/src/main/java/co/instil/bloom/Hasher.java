/**
 * Copyright 2014 Instil.
 */
package co.instil.bloom;

import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

/**
 * Simple class for handling hashing.
 */
public class Hasher {
    public HashCode murmur3Hash(String value) {
        return Hashing.murmur3_128().newHasher().putString(value, Charsets.UTF_8).hash();
    }
}
